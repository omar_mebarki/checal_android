package omar.mebarki.checal;

import omar.mebarki.checal.controller.ChecalController;
import omar.mebarki.checal.model.CheCalModel;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ScrollView;

public class CheCalActivity extends Activity {
	private static final String NUM_TEXT = "NUM_TEXT";
	private static final String NUM_DIGITS = "NUM_DIGITS";
	private ChecalController controller;
	private int orientation = Configuration.ORIENTATION_PORTRAIT;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initViewAndModel(savedInstanceState);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean result = super.onKeyDown(keyCode, event);
		if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
			controller.buttonClick(keyCode - KeyEvent.KEYCODE_0);
		} else if (keyCode == KeyEvent.KEYCODE_DEL) {
			controller.buttonClick(ChecalController.DELETE_ACTION);
		}
		return result;
	}

	private void initViewAndModel(Bundle savedInstanceState) {
		CheCalModel model = new CheCalModel();
		if (savedInstanceState != null) {
			model.setNumber(savedInstanceState.getInt(NUM_DIGITS));
			model.setText(savedInstanceState.getString(NUM_TEXT));
		} else {
			model.setNumber(0);
			model.setText(getResources().getString(R.string.txtZero));
		}
		controller = new ChecalController(model, this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(NUM_DIGITS, controller.getModel().getNumber());
		outState.putString(NUM_TEXT, controller.getModel().getText());
	}

	public void buttonClick(View view) {
		controller.buttonClick(view);
	}
}