package omar.mebarki.checal.controller;

public class DigitsTranslator {
	private StringBuilder stringBuilder;
	private static final String degits[] = { "z�ro", "un", "deux", "trois",
			"quatre", "cinq", "six", "sept", "huit", "neuf" };
	private static final String tens[] = { "dix", "onze", "douze", "treize",
			"quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf" };
	private static final String tensOthers[] = { "dix", "dix", "vingt",
			"trente", "quarante", "cinquante", "soixante", "soixante-dix",
			"quatre-vingt", "quatre-vingt-dix" };
	private static final String centString = "cent";
	private static final String spaceString = " ";
	private static final String sString = "s";
	private static final String milleString = "mille";
	private static final String millionString = "million";

	public String translateNumber(int number) {
		stringBuilder = new StringBuilder(1024);
		String result;
		if (number < 10) {
			this.one(number);
		} else if (number < 100) {
			this.ten(number, true);
		} else if (number < 1000) {
			this.hundreds(number, true);
		} else if (number < 1000000) {
			this.thousands(number);
		} else if (number < 1000000000) {
			this.millions(number);
		}
		result = stringBuilder.toString();
		stringBuilder = null;
		return result;
	}

	private void one(int number) {
		stringBuilder.append(degits[number]);
	}

	private void ten(int number, boolean displayS) {
		int div = number / 10;
		int rest = number % 10;
		if (div == 1) {
			stringBuilder.append(tens[rest]);
		} else {
			this.tensOther(number, displayS);
		}
	}

	private void tensOther(int number, boolean displayS) {
		int div = number / 10;
		int rest = number % 10;
		boolean seven_or_nine = false;

		if (((div == 7) || (div == 9))) {
			stringBuilder.append(tensOthers[div - 1]);
			seven_or_nine = true;
		} else {
			stringBuilder.append(tensOthers[div]);
		}
		if (rest == 0) {
			if (seven_or_nine == true) {
				stringBuilder.append("-");
				stringBuilder.append(tens[rest]);
			} else if (div == 8 && (displayS == true)) {
				stringBuilder.append(sString);
			}
		} else {
			if (seven_or_nine == true) {
				if ((rest == 1) && (div == 7)) {
					stringBuilder.append(" et ");
				} else {
					stringBuilder.append("-");
				}
				this.ten((rest + 10), true);
			} else {
				if ((rest == 1) && (div != 8)) {
					stringBuilder.append(" et ");
				} else {
					stringBuilder.append("-");
				}
				this.one(rest);
			}
		}

	}

	private void hundreds(int number, boolean displayS) {
		int div = number / 100;
		int rest = number % 100;
		if (div > 0) {
			if (div > 1) {
				this.one(div);
				stringBuilder.append(spaceString);
			}
			stringBuilder.append(centString);
		}

		if (rest == 0) {
			if ((div > 1) && (displayS == true)) {
				stringBuilder.append(sString);
			}
		} else {
			stringBuilder.append(spaceString);
			if (rest < 10) {
				if (rest > 0) {
					this.one(rest);
				}
			} else {
				this.ten(rest, displayS);
			}
		}

	}

	private void thousands(int number) {
		int div = number / 1000;
		int rest = number % 1000;
		if (div < 10) {
			if (div != 1) {
				this.one(div);
				stringBuilder.append(spaceString);
			}
		} else if (div < 100) {
			this.ten(div, false);
			stringBuilder.append(spaceString);
		} else {
			this.hundreds(div, false);
			stringBuilder.append(spaceString);
		}

		stringBuilder.append(milleString);

		if (rest < 10) {
			if (rest != 0) {
				stringBuilder.append(spaceString);
				this.one(rest);
			}
		} else {
			stringBuilder.append(spaceString);
			this.hundreds(rest, true);
		}

	}

	private void millions(int number) {
		int div = number / 1000000;
		int rest = number % 1000000;
		if (div < 10) {
			this.one(div);
			stringBuilder.append(spaceString);
		} else if (div < 100) {
			this.ten(div, false);
			stringBuilder.append(spaceString);
		} else if (div < 1000) {
			this.hundreds(div, false);
			stringBuilder.append(spaceString);
		} else {
			this.thousands(div);
			stringBuilder.append(spaceString);
		}

		stringBuilder.append(millionString);
		if (div != 1) {
			stringBuilder.append(sString);
		}
		if (rest < 10) {
			if (rest != 0) {
				stringBuilder.append(spaceString);
				this.one(rest);
			}
		} else if (rest < 10) {
			stringBuilder.append(spaceString);
			this.hundreds(rest, true);
		} else {
			stringBuilder.append(spaceString);
			this.thousands(rest);
		}
	}
}
