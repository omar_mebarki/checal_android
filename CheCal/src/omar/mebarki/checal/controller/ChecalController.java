package omar.mebarki.checal.controller;

import android.view.View;
import android.widget.TextView;
import omar.mebarki.checal.CheCalActivity;
import omar.mebarki.checal.R;
import omar.mebarki.checal.model.CheCalModel;

public class ChecalController {
	public static final int DELETE_ACTION = -1;
	private final CheCalModel model;
	private final CheCalActivity activity;
	private final DigitsTranslator digitsTranslator;

	public ChecalController(CheCalModel model, CheCalActivity activity) {
		this.model = model;
		this.activity = activity;
		this.digitsTranslator = new DigitsTranslator();
		refreshView();
	}

	public void refreshView() {
		TextView textViewNumber = (TextView) activity
				.findViewById(R.id.lblNumber);
		textViewNumber.setText("" + model.getNumber());

		TextView textViexText = (TextView) activity.findViewById(R.id.lblText);
		textViexText.setText(model.getText());
	}

	public void buttonClick(View view) {
		Object tag = view.getTag();
		if (tag != null) {
			String digit = tag.toString();
			int number = Integer.parseInt(digit);
			buttonClick(number);
		} else {
			buttonClick(DELETE_ACTION);
		}
	}

	public void buttonClick(int digit) {
		if (digit >= 0 && digit <= 9) {
			if (model.getNumber() == 0 && 0 == digit) {
				return;
			}
			if (model.getNumber() < 100000000) {
				int currentNumber = digit + model.getNumber() * 10;
				String texte = digitsTranslator.translateNumber(currentNumber);
				model.setNumber(currentNumber);
				model.setText(texte);

				refreshView();
			}
		} else if (digit == DELETE_ACTION) {
			delete();
		}
	}

	private void delete() {
		model.setNumber(0);
		model.setText(activity.getResources().getString(R.string.txtZero));
		refreshView();
	}

	public CheCalModel getModel() {
		return model;
	}

}
